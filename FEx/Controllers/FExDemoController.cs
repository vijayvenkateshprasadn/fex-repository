﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Web.Mvc;

namespace FEx.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class FExDemoController : Controller
    {
       
        
        public ActionResult Index()
        {
            return View();
        }

        #region GetData


        /// <summary>
        /// Gets the foldertree
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public JsonResult GetFolderTree(string path)
        {
            try
            {
                FillResourceTree(path);
            }
            catch (Exception)
            {
                return Json(new { files = lstFiles });
            }

            return Json(new { files = lstFiles });
        }

        /// <summary>
        /// Fills the resource tree
        /// </summary>
        /// <param name="path"></param>
        private void FillResourceTree(string path)
        {            
            var files = FillResources(path);
            lstFiles.AddRange(files);
        }

        /// <summary>
        /// Fills the Files and folder data
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private List<Resource> FillResources(string path)
        {
            var resources = new List<Resource>();
           
            try
            {
                
                var files = GetFiles(path);
                foreach (var item in files)
                {
                    resources.Add(new Resource()
                    {
                        Name = item.Name.Split('.')[0],
                        Path = item.FullName,
                        Type = File,
                        Childs = null,
                        CreatedDate = item.CreationTime.ToShortDateString(),
                        LastModifiedDate = item.LastWriteTime.ToShortDateString(),
                        Format = GetFormat(item.FullName)
                    });
                }

                var directories = Directory.GetDirectories(path, "*.*");


                foreach (var item in directories)
                {
                    var name = item.Replace("\\", "/").Replace("//", "/");
                    var nameAry = item.Replace("\\", "/").Replace("//", "/").Split('/');

                    resources.Add(new Resource()
                    {
                        Name = nameAry[nameAry.Length - 1],
                        Path = name,
                        Type = Folder,
                        Childs = FillResourcesSecondLevel(item),
                        CreatedDate = Directory.GetCreationTime(item).ToShortDateString(),
                        LastModifiedDate = Directory.GetLastAccessTime(item).ToShortDateString(),
                        Format = Folder
                    });
                }
            }
            catch (Exception)
            {

                return resources;
            }

            return resources;
        }

        /// <summary>
        /// Fills the files and folders of the folders
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private List<Resource> FillResourcesSecondLevel(string path)
        {
            var resources = new List<Resource>();

            try
            {

                var files = GetFiles(path);
                foreach (var item in files)
                {
                    resources.Add(new Resource()
                    {
                        Name = item.Name.Split('.')[0],
                        Path = item.FullName,
                        Type = File,
                        Childs = null,
                        CreatedDate = item.CreationTime.ToShortDateString(),
                        LastModifiedDate = item.LastWriteTime.ToShortDateString(),
                        Format = GetFormat(item.FullName)
                    });
                }

                var directories = Directory.GetDirectories(path, "*.*");


                foreach (var item in directories)
                {
                    var name = item.Replace("\\", "/").Replace("//", "/");
                    var nameAry = item.Replace("\\", "/").Replace("//", "/").Split('/');

                    resources.Add(new Resource()
                    {
                        Name = nameAry[nameAry.Length - 1],
                        Path = name,
                        Type = Folder,
                        Childs = null,
                        CreatedDate = Directory.GetCreationTime(item).ToShortDateString(),
                        LastModifiedDate = Directory.GetLastAccessTime(item).ToShortDateString(),
                        Format = Folder
                    });
                }
            }
            catch (Exception)
            {

                return resources;
            }

            return resources;
        }

        /// <summary>
        /// Gets the Format of the file
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        private string GetFormat(string fullPath)
        {
            var type = fullPath.Split('.')[fullPath.Split('.').Length - 1];
            return type;
        }

        /// <summary>
        /// Get the files in the folder
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private List<FileInfo> GetFiles(string path)
        {
            var files = new List<FileInfo>();

            var filesPaths = Directory.GetFiles(path, "*.*");
            for (int i = 0; i < filesPaths.Length; i++)
            {
                var fileInfo = new FileInfo(filesPaths[i]);
                files.Add(fileInfo);
            }

            return files;
        }

        #endregion

        #region FileActions

        /// <summary>
        /// Saves the new added files
        /// </summary>
        /// <returns></returns>
        public bool SaveFile()
        {
            Thread.Sleep(2500);
            foreach (string upload in Request.Files)
            {
                string path = Request.Params[FilePath];
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(path, filename));
            }
            return true;
        }

        /// <summary>
        /// Updates the files as per the actions specified
        /// </summary>
        /// <param name="updateType"></param>
        /// <param name="path"></param>
        /// <param name="pathNew"></param>
        /// <param name="paths"></param>
        /// <returns></returns>
        public bool UpdateFolder(string updateType, string path, string pathNew, string[] paths)
        {
            if (updateType.ToLower() == Delete)
            {
                if (paths.Length > 0)
                {
                    foreach (var item in paths)
                    {
                        System.IO.File.Delete(item);
                    }

                }
            }


            if (updateType.ToLower() == Newfolder)
                Directory.CreateDirectory(path);

            if (updateType.ToLower() == Rename)
                Directory.Move(path, pathNew);

            if (updateType.ToLower() == Move)
            {
                System.IO.File.Copy(path, pathNew);
            }
            if (updateType.ToLower() == New)
            {
                foreach (string upload in Request.Files)
                {
                    string filename = Path.GetFileName(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(path, filename));
                }
            }
            return true;
        }

        #endregion


        #region Variables

        private List<Resource> lstFiles = new List<Resource>();

        private const string New = "new";

        private const string Move = "move";

        private const string Rename = "rename";

        private const string Newfolder = "newfolder";

        private const string Delete = "delete";

        private const string FilePath = "path";

        private const string Folder = "Folder";

        private const string File = "File";

        private const int Two = 2;

        #endregion

        /// <summary>
        /// Contract for Resource data
        /// Format should be the file format i.e. PDF,Docx,TXT... and Childs should be child values.. Rest can be changed
        /// </summary>
        private class Resource
        {
           

            public string Type { get; set; }

            public string Path { get; set; }

            public string Name { get; set; }

            public string Format { get; set; }

            public string LastModifiedDate { get; set; }

            public string LastModifiedBy { get; set; }

            public string CreatedDate { get; set; }

            public string CreatedBy { get; set; }            

            public List<Resource> Childs { get; set; }
        }

    }
}
